#include <SPI.h>
#include <Ethernet.h>

#define MAC {  0x90, 0xA2, 0xDA, 0x0D, 0x00, 0xD1 }
#define IP { 192, 168, 1, 156 }
#define LISTENING_PORT 5005
#define MAX_SIZE 64

byte mac[] = MAC;
byte ip[] = IP; 

byte buffer [MAX_SIZE];

EthernetServer server = EthernetServer(LISTENING_PORT);

void setup()
{
  Serial.begin(9600);
  Ethernet.begin(mac, ip);
  Serial.print("IP Address is: ");
  for (int i = 0; i < 4; i++)
  {
    Serial.print(ip[i]);
    Serial.print((i!=3) ? "." : "");
  }
  Serial.println();
  
  server.begin();
  Serial.print("Listening in port: ");
  Serial.println(LISTENING_PORT);
}

void loop()
{
  EthernetClient client = server.available();
  if (client == true) {
    int n = client.read(buffer, MAX_SIZE);
    for (int i = 0; i < n; i++)
    {
      Serial.print((char)(buffer[i]));
    }
    server.write(buffer, n);
  }
}
