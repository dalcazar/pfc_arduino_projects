#include <SPI.h>
#include <Ethernet.h>
#include <Servo.h> 

#define MAC {  0x90, 0xA2, 0xDA, 0x0D, 0x00, 0xD1 }
#define IP { 192, 168, 1, 156 }
#define LISTENING_PORT 5005
#define MAX_SIZE 64
#define POSITION_STEP 5

byte mac[] = MAC;
byte ip[] = IP; 

byte buffer [MAX_SIZE];

Servo servoArribaAbajo;
Servo servoIzquierdaDerecha;

int posicionServoArribaAbajo = 0;
int posicionServoIzquierdaDerecha = 0;

EthernetServer server = EthernetServer(LISTENING_PORT);

void setup()
{
  Serial.begin(9600);
  Ethernet.begin(mac, ip);
  Serial.print("IP Address is: ");
  for (int i = 0; i < 4; i++)
  {
    Serial.print(ip[i]);
    Serial.print((i!=3) ? "." : "");
  }
  Serial.println();
  
  server.begin();
  Serial.print("Listening in port: ");
  Serial.println(LISTENING_PORT);
  
  servoArribaAbajo.attach(9);
  servoArribaAbajo.write(posicionServoArribaAbajo);
  servoIzquierdaDerecha.attach(8);
  servoIzquierdaDerecha.write(posicionServoIzquierdaDerecha);
}

void loop()
{
  EthernetClient client = server.available();
  if (client == true) {
    int n = client.read(buffer, MAX_SIZE);
    for (int i = 0; i < n; i++)
    {
      Serial.print((char)(buffer[i]));
    }
    if ((char)(buffer[0]) == '0')
    {
      Serial.print("0 reveived. posicionServoArribaAbajo = ");
      Serial.print(posicionServoArribaAbajo);
      if (posicionServoArribaAbajo + POSITION_STEP <= 180)
      {
        posicionServoArribaAbajo += POSITION_STEP;
        Serial.print(". Moving servoArribaAbajo servo to ");
        Serial.println(posicionServoArribaAbajo);
        servoArribaAbajo.write(posicionServoArribaAbajo);
      }
    }
    else if ((char)(buffer[0]) == '1')
    {
      Serial.print("1 reveived. posicionServoIzquierdaDerecha = ");
      Serial.print(posicionServoIzquierdaDerecha);
      if (posicionServoIzquierdaDerecha + POSITION_STEP <= 180)
      {
        posicionServoIzquierdaDerecha += POSITION_STEP;
        Serial.print(". Moving servoIzquierdaDerecha servo to ");
        Serial.println(posicionServoIzquierdaDerecha);
        servoIzquierdaDerecha.write(posicionServoIzquierdaDerecha);
      }
    }
    else if ((char)(buffer[0]) == '2')
    {
      Serial.print("1 reveived. posicionServoIzquierdaDerecha = ");
      Serial.print(posicionServoIzquierdaDerecha);
      if (posicionServoIzquierdaDerecha - POSITION_STEP >= 0)
      {
        posicionServoIzquierdaDerecha -= POSITION_STEP;
        Serial.print(". Moving servoIzquierdaDerecha servo to ");
        Serial.println(posicionServoIzquierdaDerecha);
        servoIzquierdaDerecha.write(posicionServoIzquierdaDerecha);
      }
    }
    else if ((char)(buffer[0]) == '3')
    {
      Serial.print("1 reveived. posicionServoArribaAbajo = ");
      Serial.print(posicionServoArribaAbajo);
      if (posicionServoArribaAbajo - POSITION_STEP >= 0)
      {
        posicionServoArribaAbajo -= POSITION_STEP;
        Serial.print(". Moving servoArribaAbajo servo to ");
        Serial.println(posicionServoArribaAbajo);
        servoArribaAbajo.write(posicionServoArribaAbajo);
      }
    }
   
    server.write(buffer, n);
  }
}
