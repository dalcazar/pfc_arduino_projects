#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>

#define MAC {  0x90, 0xA2, 0xDA, 0x0D, 0x00, 0xD1 }
#define IP { 192, 168, 1, 156 }
#define LISTENING_PORT 5000
#define MAX_SIZE 64

byte mac[] = MAC;
byte ip[] = IP; 

byte buffer [MAX_SIZE]; 

File logFile;
const int chipSelect = 4;

EthernetServer server = EthernetServer(LISTENING_PORT);


void setup()
{
  Serial.begin(9600);
  Ethernet.begin(mac, ip);
  Serial.print("IP Address is: ");
  for (int i = 0; i < 4; i++)
  {
    Serial.print(ip[i]);
    Serial.print((i!=3) ? "." : "");
  }
  Serial.println();
  
  server.begin();
  Serial.print("Listening in port: ");
  Serial.println(LISTENING_PORT);
  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
  pinMode(10, OUTPUT);
   
  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("SD initialization done.");
  
  logFile = SD.open("socket.log", FILE_WRITE);
  Serial.println("Log File ready.");
}

void loop()
{
  EthernetClient client = server.available();
  if (client == true) {
    int n = client.read(buffer, MAX_SIZE);
    for (int i = 0; i < n; i++)
    {
      Serial.print((char)(buffer[i]));
    }
    server.write(buffer, n);
    logFile.write(buffer, n);
    logFile.write(13);//Meto un retorno de carro, por si acaso 
                      //el mensaje no viene con uno, para que luego se pueda leer bien el fichero
    logFile.flush();    
  }
}
