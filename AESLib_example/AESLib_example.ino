//PRUEBA ENCRIPTADO USANDO LIBRERIA AES V.2
//27/9/2012
#include <AESLib.h>

//Texto original
/*byte plain [] = "abcdefghijklmnop";

byte key []   = 
  { 'T', 'h', 'e', 'B', 'e', 's', 't',
	'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y' } ;*/

void loop () 
{}


void setup ()
{
  Serial.begin (9600);
  uint8_t key []   = 
  { 'T', 'h', 'e', 'B', 'e', 's', 't',
	'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y' } ;
  byte data[] = "abcdefghijklmnop"; //16 chars == 16 bytes
  Serial.print("original:");
  Serial.println();
  Serial.print ("BYTE = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i]);
    Serial.print(" ");
  }
  Serial.println();
  Serial.print ("HEX = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  aes128_enc_single(key, data);
  Serial.print("encrypted:");
  Serial.println();
  Serial.print ("BYTE = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i]);
    Serial.print(" ");
  }
  Serial.println();
  Serial.print ("HEX = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  aes128_dec_single(key, data);
  Serial.print("decrypted:");
  Serial.println();
  Serial.print ("BYTE = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i]);
    Serial.print(" ");
  }
  Serial.println();
  Serial.print ("HEX = ") ;
  for (int i = 0; i<17; i++)
  {
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
}

