#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <AESLib.h>
#include <string.h>

#define MAC {  0x90, 0xA2, 0xDA, 0x0D, 0x00, 0xD1 }
#define IP { 192, 168, 1, 156 }
#define LISTENING_PORT 5001
#define MAX_SOCKET_SIZE 64
#define MAX_AES_SIZE 16
#define KEY { 'P', 'F', 'C', 't', 'r', 'i', 'p', 'l', 'e', 'A', 'D','a', 'v', 'i', 'd', '.' }

byte mac[] = MAC;
byte ip[] = IP; 

byte socket_read_buffer [MAX_SOCKET_SIZE];
byte socket_write_buffer [MAX_SOCKET_SIZE];
byte aes_buffer [MAX_AES_SIZE]; 

uint8_t key [] = KEY;

File logFile;
const int chipSelect = 4;

EthernetServer server = EthernetServer(LISTENING_PORT);

void setup()
{
  Serial.begin(9600);
  Ethernet.begin(mac, ip);
  Serial.print("IP Address is: ");
  for (int i = 0; i < 4; i++)
  {
    Serial.print(ip[i]);
    Serial.print((i!=3) ? "." : "");
  }
  Serial.println();
  
  server.begin();
  Serial.print("Listening in port: ");
  Serial.println(LISTENING_PORT);
  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
  pinMode(10, OUTPUT);
     
  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("SD initialization done.");
  
  logFile = SD.open("socket.log", FILE_WRITE);
  Serial.println("Log File ready.");  
}

void loop()
{
  EthernetClient client = server.available();
  if (client == true) {
    int bitsRead = 0;
    int n = 0;
    int isValid = 0;
    int numberOfValidBits = 0;
    
    memset(socket_read_buffer, 0, MAX_SOCKET_SIZE);
    do
    {
      n = client.read(socket_read_buffer + bitsRead, MAX_SOCKET_SIZE - bitsRead);
      if (n != -1)
      {
        bitsRead += n;
      }
    }
    while (bitsRead < MAX_SOCKET_SIZE);
    
    memset(socket_write_buffer, 0, MAX_SOCKET_SIZE);
    
    for (int i = 0; i < MAX_SOCKET_SIZE / MAX_AES_SIZE; i++)
    {
      memset(aes_buffer, 0, MAX_AES_SIZE);
      memcpy(aes_buffer, socket_read_buffer + i * MAX_AES_SIZE, MAX_AES_SIZE);
      
      isValid = 0;
      for (int i = 0; i < MAX_AES_SIZE; i++)
      {
        if (aes_buffer[i] != (byte)0)
        {
          isValid = 1;
        }
      }
      
      if (isValid)
      {
        aes128_dec_single(key, aes_buffer);
        
        numberOfValidBits = MAX_AES_SIZE;
        for (int i = 0; i < MAX_AES_SIZE; i++)
        {
          Serial.print((char)(aes_buffer[i]));
          if (aes_buffer[i] == 0)
          {
            numberOfValidBits = i;
            i = MAX_AES_SIZE;
          }          
        }
        Serial.println();
        
        logFile.write(aes_buffer, numberOfValidBits);
        logFile.flush(); 
        
        aes128_enc_single(key, aes_buffer);
      }
      
      memcpy(socket_write_buffer + i * MAX_AES_SIZE, aes_buffer, MAX_AES_SIZE);
    }
    server.write(socket_write_buffer, MAX_SOCKET_SIZE);
  }
}
